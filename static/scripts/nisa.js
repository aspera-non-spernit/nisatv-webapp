define(["nisa"], function(nisa) {
    return {
        Attendence: class Attendence {
            constructor(count, source) {
                this.count = count;
                this.source = source;
            }
        },
        Broadcast: class Broadcast {
            constructor(id, commentators, video_link) {
                this.id = id;
                this.commentators = commentators;
                this.video_link = video_link;
            }
        },
        Club: class Club {
            constructor(id, short, name, colors) {
                this.id = id;
                this.short = short;
                this.name = name;
                this.colors = colors;
            }
        },
        Colors: class Colors {
            constructor(primary, secondary, tertiary) {
                this.primary = primary;
                this.secondary = secondary;
                this.tertiary = tertiary;
            }
        },
        Match: class Match {
            constructor(id, home, away, date, attendence, broadcasts) {
                this.id = id;
                this.home = home;
                this.away = away;
                this.date = date;
                this.attendence = attendence;
                this.broadcasts = broadcasts;
            }
        },
        VideoPlayer: class VideoPlayer {
            constructor(screen) {
                this.screen = screen;
            }
            play(content) {
                $(this.screen).attr("src", content);
            }
            /*
            next_by_date(playlist, date) {
                for (i = 0; i < playlist.length; i++) {
                    console.log(playlist[i].date);
            }
     
            }
            prev_by_date(playlist, date) {
                playlist.sort(
					function(a, b) {
						return b.date - a.date }
				);
                for (var m in playlist) {
                    if (playlist[m].date < date) {
						return playlist[m];
		    		}
                }
            }
            */
        }
    }
});




