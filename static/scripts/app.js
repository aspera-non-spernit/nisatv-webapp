require(["nisa"], function(nisa) {
    $(document).ready(function() {      
        resize();
        var nisa = require("./nisa");
        var active = $(".schedule").attr("id") ; // + "-icon-link";
        var player = new nisa.VideoPlayer("#video");
        let matches = [];
        var current_match;
        var next_match;
    
        function resize() {
            if ( $(window).width() > $(window).height() ) {          
                var height = $(window).height() - $("#header").height() - $("#controls").height() - $("#footer").height() - $("#footer").height();
                $("#video").css("height", height);
                $("#video").css("width", height * 1.67);
                $("#screen").css("height", height);
                $("#screen").css("width", height * 1.67);
            } 
            else if ( $(window).height() > $(window).width() ) {
                var width = $(window).width();          
                $("#video").css("width", width);
                $("#video").css("height", width / 1.67);
                $("#screen").css("width", width);
                $("#screen").css("height", width / 1.67);
            }
        }
        $(window).on("resize", function() {
            resize();
        });
        $("#show-prev-btn").click(function() {
            let now = new Date();
            $("#match-1").css("display", "hidden");

        })
    }); // document ready
}); // require