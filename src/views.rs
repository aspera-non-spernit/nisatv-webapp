use crate::utils::{
    club_by_trivial, find_club_by_short, find_match, get_players, trivial_from_path,
};
use chrono::{DateTime, Local};
use nisatv_models::{Club, Match, Player, Position, site::nav::IconMenu};
use serde::Serialize;

#[derive(Clone, Serialize)]
pub struct ClubOverviewView {
    pub main_menu: Option<IconMenu>,
    pub club: Option<Club>,
    pub context: Option<String>,
    pub scripts: Vec<String>,
}

impl ClubOverviewView {
    pub fn new(main_menu: Option<IconMenu>, filter: &str, scripts: &Vec<String>, clubs: &Vec<Club>) -> Self {
        let club = club_by_trivial(trivial_from_path(&filter), clubs);
        let context = Some(trivial_from_path(filter).replace(" ", "-").to_lowercase());
        ClubOverviewView {
            main_menu,
            club,
            context,
            scripts: scripts.clone(),
        }
    }
}

#[derive(Clone, Serialize)]
pub struct ClubPlayersView {
    pub main_menu: Option<IconMenu>,
    pub club: Option<Club>,
    pub context: Option<String>,
    pub players: Option<Vec<Player>>,
    pub scripts: Vec<String>,
}

impl ClubPlayersView {
    pub fn new(
        main_menu: Option<IconMenu>,
        filter: &str,
        clubs: &Vec<Club>,
        scripts: &Vec<String>,
        position: Option<Position>,
    ) -> Self {
        let club = club_by_trivial(trivial_from_path(&filter), clubs);
        let context = Some(trivial_from_path(&filter).replace(" ", "-").to_lowercase());
        let mut players = match filter {
            "detroit-city-fc" => {
                get_players("http://radiant-citadel-22556.herokuapp.com/api/players")
            }
            "chattanooga-fc" => {
                get_players("https://chattahooligan-hymnal.herokuapp.com/api/players")
            }
            _ => None,
        };
        if let Some(p) = position {
            players = ClubPlayersView::filter(&players.unwrap(), p);
        }
        ClubPlayersView {
            main_menu,
            club,
            context,
            players,
            scripts: scripts.to_vec(),
        }
    }

    fn filter(players: &Vec<Player>, position: Position) -> Option<Vec<Player>> {
        let filtered = players
            .iter()
            .cloned()
            .filter(|p| p.position == position)
            .collect::<Vec<Player>>();
        if filtered.len() != 0 {
            Some(filtered)
        } else {
            None
        }
    }
}

#[derive(Clone, Serialize)]
pub struct View {
    pub main_menu: Option<IconMenu>,
    pub requested: Option<Club>,
    pub clubs: Option<Vec<Club>>,
    pub matches: Option<Vec<Match>>,
    pub context: Option<String>,
    pub scripts: Vec<String>,
}

#[derive(Clone, Serialize)]
pub struct WatchMatchView {
    pub main_menu: Option<IconMenu>,
    pub home: Option<Club>,
    pub away: Option<Club>,
    pub m: Option<Match>,
    pub context: Option<String>,
    pub scripts: Vec<String>,
}

impl WatchMatchView {
    pub fn new(
        main_menu: Option<IconMenu>,
        home: &str,
        vs: &str,
        date: &str,
        clubs: &Vec<Club>,
        matches: &Vec<Match>,
        scripts: Vec<String>,
    ) -> Self {
        let d = date.replace(" ", "+");
        let m = find_match(
            matches,
            &DateTime::parse_from_rfc3339(&d).unwrap(),
            &home,
            &vs,
        );
        let home = find_club_by_short(clubs, home);
        let away = find_club_by_short(clubs, &vs);
        WatchMatchView {
            main_menu,
            home,
            away,
            m,
            context: None,
            scripts,
        }
    }
}
