use ::std::{
    fs::File,
    io::{BufRead, BufReader, Read},
};

/// Load and bundle Javascripts from a config file
#[derive(Clone, Debug)]
pub struct BundleConfig {
    pub path: String,
    pub sources: Vec<String>,
    pub to_file: bool,
}

impl BundleConfig {
    pub fn new(path: &str, to_file: bool) -> Self {
        match BundleConfig::sources(&path) {
            Ok(sources) => BundleConfig {
                path: path.to_string(),
                sources,
                to_file,
            },
            Err(e) => panic!("{}", e),
        }
    }

    fn sources(path: &str) -> std::io::Result<Vec<String>> {
        let f = File::open(format!("{}/config/bundle.cfg", path))?;
        let reader = BufReader::new(f);
        reader.lines().collect()
    }
}

#[derive(Clone, Debug)]
pub struct Bundle {
    pub config: BundleConfig,
    pub scripts: Vec<String>,
}

impl Bundle {
    pub fn new(config: BundleConfig) -> Self {
        let mut scripts: Vec<String> = vec![];
        for source in &config.sources {
            match File::open(format!("{}/static/{}", &config.path, source)) {
                Ok(f) => {
                    let mut reader = BufReader::new(f);
                    // scripts.push( reader.lines().map(|l| l.unwrap()).collect() );
                    let mut buf = String::new();
                    reader.read_to_string(&mut buf);
                    scripts.push(buf);
                }
                Err(e) => {
                    println!("{:?}", e);
                }
            }
        }
        Bundle { config, scripts }
    }
}
