use crate::NisaError;
use chrono::{DateTime, FixedOffset, Local};
use nisatv_models::{Club, Match, Player};

/// from data source
pub fn fetch_match(
    data_source: &str,
    date: DateTime<FixedOffset>,
    home: &str,
    away: &str,
) -> Option<Match> {
    let qs = format!("/match?by_date={}", date.to_string());
    if let Ok(matches) = get_matches(&qs, data_source) {
        matches
            .iter()
            .find(|&m| home == m.home_short && away == m.away_short)
            .cloned()
    } else {
        None
    }
}
/// Retrieves matches from data_source
// in this app only used onced to create the managed State NisaState
pub fn get_matches(query: &str, data_source: &str) -> Result<Vec<Match>, NisaError> {
    let qs = format!("{}{}", data_source, query);
    match reqwest::get(&qs) {
        Ok(mut resp) => match resp.text() {
            Ok(text) => {
                let matches: Result<Vec<Match>, serde_json::error::Error> =
                    serde_json::from_str(&text);
                match matches {
                    Ok(mut m) => {
                        m.sort();
                        m.reverse();
                        Ok(m)
                    }
                    Err(_e) => Err(NisaError),
                }
            }
            Err(_e) => Err(NisaError),
        },
        Err(_e) => Err(NisaError),
    }
}

/// find a particular Club given its short code
pub fn find_club_by_short(clubs: &[Club], short: &str) -> Option<Club> {
    clubs.iter().find(|&c| short == c.short).cloned()
}

/// find a particular match given its date, home and away team
pub fn find_match(
    matches: &[Match],
    date: &DateTime<FixedOffset>,
    home: &str,
    away: &str,
) -> Option<Match> {
    if let Some(matches) = find_matches_by_date(matches, date) {
        matches
            .iter()
            .cloned()
            .find(|c| c.home_short == home && c.away_short == away)
    } else {
        None
    }
}

/// find a particular match given its date, home and away team
pub fn find_matches_by_date(matches: &[Match], date: &DateTime<FixedOffset>) -> Option<Vec<Match>> {
    let mut f = vec![];
    for m in matches {
        if &m.date == date {
            f.push(m.clone())
        }
    }
    if f.len() == 0 {
        None
    } else {
        Some(f)
    }
}

/// finds all matches of a Club, either at home or away given its short code
pub fn find_matches_by_short(matches: &[Match], short: &str) -> Option<Vec<Match>> {
    let mut f = vec![];
    for m in matches {
        if short == m.home_short || short == m.away_short {
            f.push(m.clone());
        }
    }
    if f.len() == 0 {
        None
    } else {
        Some(f)
    }
}

// finds all matches of a Club, either at home or away
pub fn find_matches_by_club(matches: &[Match], club: &Club) -> Option<Vec<Match>> {
    let mut f = vec![];
    for m in matches {
        if club.short == m.home_short || club.short == m.away_short {
            f.push(m.clone());
        }
    }
    if f.len() == 0 {
        None
    } else {
        Some(f)
    }
}

pub fn next(from: DateTime<FixedOffset>, matches: &[Match]) -> Option<Match> {
    let mut dates = vec![];
    for m in matches {
        if m.date > from {
            dates.push(m.date);
        }
    }
    dates.sort();
    let mut next = None;
    for m in matches {
        if let true = m.date == dates[0] {
            next = Some(m.clone())
        }
    }
    next
}

pub fn club_by_trivial(trivial: String, clubs: &[Club]) -> Option<Club> {
    clubs
        .iter()
        .cloned()
        .find(|c| c.trivial_name.to_lowercase() == trivial.to_lowercase())
        .clone()
}

/// Retrieves clubs from data_source
/// Actually not needed, as NisaState stores all Clubs on startup
pub fn get_clubs(query: &str, data_source: &str) -> Result<Vec<Club>, NisaError> {
    let qs = format!("{}{}", data_source, query);
    match reqwest::get(&qs) {
        Ok(mut resp) => match resp.text() {
            Ok(text) => match serde_json::from_str(&text) {
                Ok(m) => Ok(m),
                Err(_e) => Err(NisaError),
            },
            Err(_e) => Err(NisaError),
        },
        Err(_e) => Err(NisaError),
    }
}

/// Retrieves Players from data_source
/// Actually not needed, as NisaState stores all Clubs on startup
pub fn get_players(data_source: &str) -> Option<Vec<Player>> {
    match reqwest::get(data_source) {
        Ok(mut resp) => match resp.text() {
            Ok(text) => match serde_json::from_str(&text) {
                Ok(p) => Some(p),
                Err(e) => {
                    println!("{:?}", e);
                    None
                }
            },
            Err(e) => {
                println!("{:?}", e);
                None
            }
        },
        Err(e) => {
            println!("{:?}", e);
            None
        }
    }
}
/// ie.: ../detroit-city-fc/.. -> "detroit city fc"
pub fn trivial_from_path(path: &str) -> String {
    path.to_string()
        .split("-")
        .collect::<Vec<&str>>()
        .iter()
        .cloned()
        .fold("".to_string(), |mut t, s| {
            t.push_str(" ");
            t.push_str(s);
            t
        })
        .trim()
        .to_string()
}

pub fn local_to_fixed_offset(dt: DateTime<Local>) -> DateTime<FixedOffset> {
    dt.with_timezone(dt.offset())
}

pub fn from(matches: &[Match], dt: DateTime<Local>) -> Option<Vec<Match>> {
    let off = dt.with_timezone(dt.offset());
    let mut upcoming_matches = vec![];
    for m in matches {
        if m.date > off {
            upcoming_matches.push(m.clone());
        }
    }

    if upcoming_matches.len() == 0 {
        None
    } else {
        upcoming_matches.reverse();
        Some(upcoming_matches)
    }
}
pub fn until(matches: &[Match], dt: DateTime<Local>) -> Option<Vec<Match>> {
    let off = dt.with_timezone(dt.offset());
    let mut prevous_matches = vec![];
    for m in matches {
        if m.date < off {
            prevous_matches.push(m.clone());
        }
    }

    if prevous_matches.len() == 0 {
        None
    } else {
        prevous_matches.reverse();
        Some(prevous_matches)
    }
}

