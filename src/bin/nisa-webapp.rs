#![feature(proc_macro_hygiene, decl_macro)]
extern crate handlebars;
extern crate nisatv_models;
extern crate nisatv_webapp;
#[macro_use]
extern crate rocket;
extern crate reqwest;
#[macro_use]
extern crate serde;
extern crate tarpit;

use chrono::{DateTime, Local};
use handlebars::*;
use nisatv_models::*;
use nisatv_webapp::{
    bundle::{Bundle, BundleConfig},
    utils::{
        club_by_trivial, find_club_by_short, find_match, find_matches_by_club, from, get_clubs,
        get_matches, get_players, trivial_from_path, until,
    },
    views::{ClubOverviewView, ClubPlayersView, View, WatchMatchView},
    NisaError,
};
use rocket::{
    response::{NamedFile, Redirect, Stream},
    Request, State,
};
use rocket_contrib::{serve::StaticFiles, templates::Template};
use std::path::Path;
use tarpit::{Tarpit, TarpitReader};

#[derive(Clone, Debug, Serialize)]
pub struct NisaState {
    pub data_source: String,
    pub clubs: Vec<Club>,
    pub matches: Vec<Match>,
    pub scripts: Vec<String>,
    pub app_dir: String,
}

#[get("/")]
fn index(state: State<NisaState>) -> Template {
    let mm = if let Ok(im) = site::nav::IconMenu::read() {
        Some(im)
    } else { None };
    
    let mm = if let Ok(im) = site::nav::IconMenu::read() {
        Some(im)
    } else { None };
    Template::render(
        "index",
        View {
            main_menu: mm,
            requested: None,
            clubs: None,
            matches: None,
            context: None, // first level path
            scripts: state.clone().scripts
        },
    )
}

#[get("/<filter>/overview")]
fn club_overview(filter: String, state: State<NisaState>) -> Template {
    let mm = if let Ok(im) = site::nav::IconMenu::read() {
        Some(im)
    } else { None };
    Template::render(
        "club_overview",
        ClubOverviewView::new(mm, &filter, &state.scripts, &state.clubs),
    )
}

#[get("/<filter>/players")]
fn players(filter: String, state: State<NisaState>) -> Template {
    let main_menu = if let Ok(im) = site::nav::IconMenu::read() {
        Some(im)
    } else { None };
    Template::render(
        "club_players",
        ClubPlayersView::new(main_menu, &filter, &state.clubs, &state.scripts, None),
    )
}

#[get("/<filter>/players/defenders")]
fn players_defenders(filter: String, state: State<NisaState>) -> Template {
    let main_menu = if let Ok(im) = site::nav::IconMenu::read() {
        Some(im)
    } else { None };
    Template::render(
        "club_players",
        ClubPlayersView::new(
            main_menu,
            &filter,
            &state.clubs,
            &state.clone().scripts,
            Some(Position::Defender),
        ),
    )
}
#[get("/<filter>/players/forwards")]
fn players_forwards(filter: String, state: State<NisaState>) -> Template {
    let main_menu = if let Ok(im) = site::nav::IconMenu::read() {
        Some(im)
    } else { None };
    Template::render(
        "club_players",
        ClubPlayersView::new(
            main_menu,
            &filter,
            &state.clubs,
            &state.scripts,
            Some(Position::Forward),
        ),
    )
}

#[get("/<filter>/players/goalkeepers")]
fn players_goalkeepers(filter: String, state: State<NisaState>) -> Template {
    let main_menu = if let Ok(im) = site::nav::IconMenu::read() {
        Some(im)
    } else { None };
    Template::render(
        "club_players",
        ClubPlayersView::new(
            main_menu,
            &filter,
            &state.clubs,
            &state.scripts,
            Some(Position::Goalkeeper),
        ),
    )
}
#[get("/<filter>/players/midfielders")]
fn players_midfielders(filter: String, state: State<NisaState>) -> Template {
    let main_menu = if let Ok(im) = site::nav::IconMenu::read() {
        Some(im)
    } else { None };
    Template::render(
        "club_players",
        ClubPlayersView::new(
            main_menu,
            &filter,
            &state.clubs,
            &state.scripts,
            Some(Position::Midfielder),
        ),
    )
}

#[get("/<filter>/matches")]
fn matches(filter: String, state: State<NisaState>) -> Template {
    let mut context = None;
    let rm = if filter == "all" {
        (
            None,                                // no club
            Some(state.inner().matches.clone()), // all matches
        )
    } else {
        context = Some(filter.clone());
        if let Some(club) = club_by_trivial(trivial_from_path(&filter), &state.clubs) {
            (
                Some(club.clone()),
                find_matches_by_club(&state.matches, &club),
            )
        } else {
            (
                None, // club not available
                None, // no matches
            )
        }
    };
    let main_menu = if let Ok(im) = site::nav::IconMenu::read() {
        Some(im)
    } else { None };
    Template::render(
        "matches",
        View {
            main_menu,
            requested: rm.0,
            clubs: Some(state.inner().clubs.clone()),
            matches: rm.1,
            context,
            scripts: state.clone().scripts,
        },
    )
}

#[get("/<filter>/matches/upcoming")]
fn upcoming_matches(filter: String, state: State<NisaState>) -> Template {
    let mut context = None;
    let rm = if filter == "all" {
        let all_upcoming = from(&state.matches, Local::now());
        (
            None,         // no club
            all_upcoming, // all matches
        )
    } else {
        context = Some(filter.clone());
        if let Some(club) = club_by_trivial(trivial_from_path(&filter), &state.clubs) {
            if let Some(matches) = find_matches_by_club(&state.matches, &club) {
                (Some(club.clone()), from(&matches, Local::now()))
            } else {
                (
                    None, // club not available
                    None, // no matches
                )
            }
        } else {
            (
                None, // club not available
                None, // no matches
            )
        }
    };
    let main_menu = if let Ok(im) = site::nav::IconMenu::read() {
        Some(im)
    } else { None };
    Template::render(
        "matches",
        View {
            main_menu,
            requested: rm.0,
            clubs: Some(state.inner().clubs.clone()),
            matches: rm.1,
            context,
            scripts: state.clone().scripts,
        },
    )
}

#[get("/<filter>/matches/previous")]
fn previous_matches(filter: String, state: State<NisaState>) -> Template {
    let mut context = None;
    let rm = if filter == "all" {
        let all_previous = until(&state.matches, Local::now());
        (
            None,         // no club
            all_previous, // all matches
        )
    } else {
        context = Some(filter.clone());
        if let Some(club) = club_by_trivial(trivial_from_path(&filter), &state.clubs) {
            if let Some(matches) = find_matches_by_club(&state.matches, &club) {
                (Some(club.clone()), until(&matches, Local::now()))
            } else {
                (
                    None, // club not available
                    None, // no matches
                )
            }
        } else {
            (
                None, // club not available
                None, // no matches
            )
        }
    };
    let main_menu = if let Ok(im) = site::nav::IconMenu::read() {
        Some(im)
    } else { None };
    Template::render(
        "matches",
        View {
            main_menu,
            requested: rm.0,
            clubs: Some(state.inner().clubs.clone()),
            matches: rm.1,
            context,
            scripts: state.clone().scripts,
        },
    )
}

#[get("/watch/<home>?<vs>&<date>")]
fn watch(home: String, vs: String, date: String, state: State<NisaState>) -> Template {
    let main_menu = if let Ok(im) = site::nav::IconMenu::read() {
        Some(im)
    } else { None };
    Template::render(
        "watch",
        WatchMatchView::new(main_menu, &home, &vs, &date, &state.clubs, &state.matches, state.clone().scripts),
    )
}

fn get_state(data_source: &str) -> Result<NisaState, NisaError> {
    let path_buf = std::env::current_dir().expect("Couldn't determine current_dir.");
    let config = path_buf
        .to_str()
        .expect("Couldn't yield current dir to &str");
    let bundle = Bundle::new(BundleConfig::new(config, false));
    let clubs = get_clubs("/all/clubs", data_source)?;
    let matches = get_matches("/all/matches", data_source)?;
    Ok(NisaState {
        clubs,
        data_source: data_source.to_string(),
        matches,
        scripts: bundle.scripts,
        app_dir: config.to_string(),
    })
}

#[get("/robots.txt")]
fn robots(state: State<NisaState>) -> Option<NamedFile> {
    NamedFile::open(Path::new(&format!("{}/static/robots.txt", state.app_dir))).ok()
}

#[get("/sec/bog_down")]
fn bog_down() -> Stream<TarpitReader> {
    Stream::chunked(TarpitReader::new(1000), 64)
}

#[catch(404)]
fn not_found(req: &Request) -> Redirect {
    let tarpit = Tarpit::new(
        &format!(
            "{}/config/suspicious_urls",
            std::env::current_dir().unwrap().to_str().unwrap()
        ), // cannot panic
        vec![],
    );

    if tarpit.known(&req.uri().path()).is_some() {
        Redirect::to(uri!(bog_down))
    } else {
        Redirect::to(uri!(index))
    }
}

fn main() -> Result<(), NisaError> {
    let args: Vec<String> = std::env::args().collect();
    let pb = std::env::current_dir().unwrap();
    let curr_dir = pb.to_str().unwrap();
    let mut handlebars = Handlebars::new();
    handlebars.set_strict_mode(true);
    rocket::ignite()
        .register(catchers![not_found])
        .mount(
            "/",
            routes![
                bog_down,
                club_overview,
                players,
                index,
                matches,
                players_defenders,
                players_forwards,
                players_goalkeepers,
                players_midfielders,
                previous_matches,
                robots,
                upcoming_matches,
                watch
            ],
        )
        .mount(
            "/assets",
            StaticFiles::from(format!("{}/static/assets", curr_dir)),
        )
        .mount(
            "/misc",
            StaticFiles::from(format!("{}/static/misc", curr_dir)),
        )
        .mount(
            "/styles",
            StaticFiles::from(format!("{}/static/styles", curr_dir)),
        )
        .mount(
            "/scripts",
            StaticFiles::from(format!("{}/static/scripts", curr_dir)),
        )
        .manage(get_state(&args[1].clone())?)
        .attach(Template::fairing())
        .launch();
    Ok(())
}
