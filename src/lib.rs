extern crate chrono;
extern crate nisatv_models;
extern crate serde;
pub mod bundle;
pub mod utils;
pub mod views;

#[derive(Debug)]
pub struct NisaError;
